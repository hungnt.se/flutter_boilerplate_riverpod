part of 'init_di.dart';

Future<void> setupUseCaseDependencies(GetIt getIt) async {
  // UseCases - Todo
  getIt.registerSingleton<GetTodosUseCase>(
      GetTodosUseCase(getIt<ITodoRepository>()));

  // UseCases - Auth
  getIt.registerSingleton<LoginUseCase>(LoginUseCase(getIt<IAuthRepository>()));
  getIt.registerSingleton<GetUserTokenUseCase>(
      GetUserTokenUseCase(getIt<IAuthRepository>()));
  getIt.registerSingleton<SetUserTokenUseCase>(
      SetUserTokenUseCase(getIt<IAuthRepository>()));
  getIt.registerSingleton<ClearTokenUseCase>(
      ClearTokenUseCase(getIt<IAuthRepository>()));

  // UseCases - User
  getIt.registerSingleton<GetUserInfoUseCase>(
      GetUserInfoUseCase(getIt<IUserRepository>()));
}
