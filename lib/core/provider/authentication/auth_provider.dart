// Package imports:
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/provider/user/user_provider.dart';
import 'package:flutter_boilerplate_riverpod/domain/entities/user.dart';

part 'auth_provider.freezed.dart';
part 'auth_state.dart';

final authProvider = StateNotifierProvider<_AuthController, AuthState>(
  (ref) {
    final user = ref.watch(userProvider);
    return _AuthController(user);
  },
);

class _AuthController extends StateNotifier<AuthState> {
  final UserState _user;

  _AuthController(this._user) : super(AuthState.unknown()) {
    _init();
  }

  Future<void> _init() async {
    final _token = _user.token;
    if (_token.isEmpty) {
      state = const AuthState.unauthenticated();
      return;
    }
    _user.user.map(
      data: (user) {
        state = AuthState.authenticated(user: user.value, token: _token);
      },
      error: (failure) => ' ',
      loading: (value) {
        state = const AuthState.authenticating();
      },
    );
  }
}
