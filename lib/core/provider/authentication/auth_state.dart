part of 'auth_provider.dart';

@freezed
class AuthState with _$AuthState {
  factory AuthState.unknown() = _Unknown;

  const factory AuthState.authenticating() = _Authenticating;

  const factory AuthState.authenticated(
      {required User user, required String token}) = _Authenticated;

  const factory AuthState.unauthenticated() = _UnAuthenticated;
}
