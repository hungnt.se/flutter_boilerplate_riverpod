// Package imports:
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/di/init_di.dart';
import 'package:flutter_boilerplate_riverpod/domain/entities/user.dart';
import 'package:flutter_boilerplate_riverpod/domain/usecases/auth/auth_usecase.dart';
import 'package:flutter_boilerplate_riverpod/domain/usecases/user/user_usecase.dart';

part 'user_provider.freezed.dart';
part 'user_state.dart';

final userProvider = StateNotifierProvider<_UserController, UserState>(
  (ref) {
    return _UserController(
      loginUseCase: getIt<LoginUseCase>(),
      getUserTokenUseCase: getIt<GetUserTokenUseCase>(),
      setUserTokenUseCase: getIt<SetUserTokenUseCase>(),
      clearTokenUseCase: getIt<ClearTokenUseCase>(),
      getUserInfoUseCase: getIt<GetUserInfoUseCase>(),
    );
  },
);

class _UserController extends StateNotifier<UserState> {
  late LoginUseCase _loginUseCase;
  late GetUserTokenUseCase _getUserTokenUseCase;
  late SetUserTokenUseCase _setUserTokenUseCase;
  late ClearTokenUseCase _clearTokenUseCase;
  late GetUserInfoUseCase _getUserInfoUseCase;

  _UserController({
    required LoginUseCase loginUseCase,
    required GetUserTokenUseCase getUserTokenUseCase,
    required SetUserTokenUseCase setUserTokenUseCase,
    required ClearTokenUseCase clearTokenUseCase,
    required GetUserInfoUseCase getUserInfoUseCase,
  }) : super(UserState.initial()) {
    _loginUseCase = loginUseCase;
    _getUserTokenUseCase = getUserTokenUseCase;
    _setUserTokenUseCase = setUserTokenUseCase;
    _clearTokenUseCase = clearTokenUseCase;
    _getUserInfoUseCase = getUserInfoUseCase;
  }

  Future<void> login(String email, String password) async {
    final result = await _loginUseCase(
        params: AuthRequestParams(email: email, password: password));

    result.fold(
      (failure) => state = state.copyWith(token: ''),
      (token) async {
        state = state.copyWith(token: token);
        await _setUserTokenUseCase(params: TokenParams(token: token));
        await getUserInfoAction();
      },
    );
  }

  Future<User?> getUserInfoAction() async {
    if (state.token.isEmpty) {
      return null;
    }
    state = state.copyWith(user: const AsyncValue.loading());
    final result = await _getUserInfoUseCase();
    return result.fold(
      (failure) {
        state = state.copyWith(token: '');
        return null;
      },
      (user) {
        state = state.copyWith(user: AsyncValue.data(user));
        return user;
      },
    );
  }

  Future<void> logout() async {
    state = UserState.initial();
    _clearTokenUseCase();
  }
}
