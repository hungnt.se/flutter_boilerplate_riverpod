part of 'user_provider.dart';

@freezed
class UserState with _$UserState {
  const factory UserState({
    required AsyncValue<User> user,
    required String token,
  }) = _UserState;

  factory UserState.initial() =>
      const UserState(user: AsyncValue.loading(), token: '');
}
