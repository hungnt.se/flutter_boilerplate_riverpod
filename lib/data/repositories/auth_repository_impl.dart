// Package imports:
import 'package:dartz/dartz.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/logics/error_handle/failures.dart';
import 'package:flutter_boilerplate_riverpod/data/datasources/sharedpref/shared_preference_helper.dart';
import 'package:flutter_boilerplate_riverpod/domain/repositories/auth_repository.dart';
import '../datasources/remote/auth/auth_api.dart';
import 'repository_utils.dart';

class AuthRepositoryImpl extends RepositoryUtils implements IAuthRepository {
  final AuthApi _api;
  final SharedPreferenceHelper _sharedPreferenceHelper;

  AuthRepositoryImpl(this._api, this._sharedPreferenceHelper);

  @override
  Future<Either<Failure, String>> login(email, password) async {
    return await convertToEither<String>(
      () async {
        final res =
            await _api.login(RequestLoginDto(email: email, password: password));
        return res.data.token.toString();
      },
    );
  }

  @override
  Future<String> getUserToken() async {
    return await _sharedPreferenceHelper.authToken ?? "";
  }

  @override
  Future<void> clearUserToken() async {
    _sharedPreferenceHelper.removeAuthToken();
  }

  @override
  Future<void> setUserToken(String token) async {
    _sharedPreferenceHelper.saveAuthToken(token);
  }
}
