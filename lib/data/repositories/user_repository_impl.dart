// Package imports:
import 'package:dartz/dartz.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/logics/error_handle/failures.dart';
import 'package:flutter_boilerplate_riverpod/domain/entities/user.dart';
import 'package:flutter_boilerplate_riverpod/domain/repositories/user_repository.dart';
import '../datasources/remote/user/user_api.dart';
import 'repository_utils.dart';

class UserRepositoryImpl extends RepositoryUtils implements IUserRepository {
  final UserApi _api;

  UserRepositoryImpl(this._api);

  @override
  Future<Either<Failure, User>> fetchUserInfo() async {
    return await convertToEither<User>(
      () async {
        final res = await _api.getUserInfo();
        return res.data;
      },
    );
  }
}
