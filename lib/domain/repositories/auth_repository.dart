// Package imports:
import 'package:dartz/dartz.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/logics/error_handle/error_handle.dart';

abstract class IAuthRepository {
  // API methods
  Future<Either<Failure, String>> login(String email, String password);

  Future<String> getUserToken();

  Future<void> setUserToken(String token);

  Future<void> clearUserToken();
}
