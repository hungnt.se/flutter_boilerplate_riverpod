// Package imports:
import 'package:dartz/dartz.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/logics/error_handle/error_handle.dart';
import '../entities/user.dart';

abstract class IUserRepository {
  // API methods
  Future<Either<Failure, User>> fetchUserInfo();
}
