// Project imports:
import '../../repositories/auth_repository.dart';
import '../usecase.dart';

class ClearTokenUseCase implements UseCase<Future<void>, void> {
  final IAuthRepository repository;

  ClearTokenUseCase(this.repository);

  @override
  call({void params}) async {
    repository.clearUserToken();
  }
}
