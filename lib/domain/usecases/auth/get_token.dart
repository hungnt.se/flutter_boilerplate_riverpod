// Project imports:
import 'package:flutter_boilerplate_riverpod/domain/repositories/auth_repository.dart';
import '../usecase.dart';

class GetUserTokenUseCase implements UseCase<Future<String>, void> {
  final IAuthRepository repository;

  GetUserTokenUseCase(this.repository);

  @override
  call({void params}) async {
    final res = await repository.getUserToken();
    return res;
  }
}
