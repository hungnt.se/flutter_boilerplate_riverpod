// Package imports:
import 'package:dartz/dartz.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/logics/error_handle/failures.dart';
import 'package:flutter_boilerplate_riverpod/domain/repositories/auth_repository.dart';
import '../usecase.dart';

class AuthRequestParams {
  final String email;
  final String password;

  const AuthRequestParams({
    required this.email,
    required this.password,
  });
}

class LoginUseCase
    implements UseCase<Future<Either<Failure, String>>, AuthRequestParams> {
  final IAuthRepository repository;

  LoginUseCase(this.repository);

  @override
  call({required params}) async {
    final res = await repository.login(params.email, params.password);
    return res;
  }
}
