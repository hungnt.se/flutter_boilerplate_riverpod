// Project imports:
import '../../repositories/auth_repository.dart';
import '../usecase.dart';

class TokenParams {
  final String token;

  const TokenParams({
    required this.token,
  });
}

class SetUserTokenUseCase implements UseCase<Future<void>, TokenParams> {
  final IAuthRepository repository;

  SetUserTokenUseCase(this.repository);

  @override
  call({required TokenParams params}) async {
    repository.setUserToken(params.token);
  }
}
