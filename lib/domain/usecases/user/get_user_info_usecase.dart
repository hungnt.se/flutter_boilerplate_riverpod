// Package imports:
import 'package:dartz/dartz.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/logics/error_handle/failures.dart';
import '../../entities/user.dart';
import '../../repositories/user_repository.dart';
import '../usecase.dart';

class GetUserInfoUseCase
    implements UseCase<Future<Either<Failure, User>>, void> {
  final IUserRepository repository;

  GetUserInfoUseCase(this.repository);

  @override
  call({void params}) async {
    final res = await repository.fetchUserInfo();
    return res;
  }
}
