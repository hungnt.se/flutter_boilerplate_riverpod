// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:hooks_riverpod/hooks_riverpod.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/provider/authentication/auth_provider.dart';
import 'package:flutter_boilerplate_riverpod/core/provider/user/user_provider.dart';
import '../auth/login_page.dart';
import '../home/home_page.dart';

class AppStartPage extends StatefulHookConsumerWidget {
  const AppStartPage({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _AppStartPageState();
}

class _AppStartPageState extends ConsumerState<AppStartPage> {
  @override
  void initState() {
    ref.read(userProvider.notifier).getUserInfoAction();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(authProvider);
    return state.maybeWhen(
      orElse: () => const CircularProgressIndicator(),
      authenticated: (_, __) => const HomePage(),
      unauthenticated: () => const LoginPage(),
    );
  }
}
