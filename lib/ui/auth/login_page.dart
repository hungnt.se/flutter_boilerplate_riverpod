// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:hooks_riverpod/hooks_riverpod.dart';

// Project imports:
import 'package:flutter_boilerplate_riverpod/core/provider/user/user_provider.dart';

class LoginPage extends HookConsumerWidget {
  const LoginPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Center(
      child: Column(
        children: [
          const Text('hello login page'),
          ElevatedButton(
              onPressed: () {
                ref
                    .read(userProvider.notifier)
                    .login('eve.holt@reqres.in', 'cityslicka');
              },
              child: const Text('login'))
        ],
      ),
    );
  }
}
